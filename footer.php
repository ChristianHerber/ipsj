	<footer class="container-fluid" id="footer">
		<div class="container">
			<div class="row">
				<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12 text-center">
					<img src="<?php bloginfo('template_url'); ?>/img/selo-60-anos.png" alt="Igreja Presbiteriana 60 Anos" class="img-fluid">
				</div>
				<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 text-center mt15">
					<p><small>Todos os direitos reservados à Igreja Prebiteriana da Silva Jardim</small></p>
				</div>
				<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12 text-center mt15">
					<a href="https://uebidigital.com.br">
						<img src="<?php bloginfo('template_url'); ?>/img/logo-uebi.png" alt="Uébi! Agência Digital - Marketing Digital" class="img-fluid">
					</a>
				</div>
			</div>			
		</div>
	</footer>

</body>
	<script src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/scripts.js"></script>
	<?php wp_footer(); ?>
</html>