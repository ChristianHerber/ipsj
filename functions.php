<?php
	register_nav_menu('menu_topo', 'ipsj');
	add_post_type_support( 'page', 'excerpt' );

	add_theme_support( 'post-thumbnails' );

	add_image_size( 'slide', '1600', '561', false );
	add_image_size( '200x200', '200', '200', array('left','top') );