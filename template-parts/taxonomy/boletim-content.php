	<div class="container-fluid">
		<div class="container content-tax">
			<div class="row">

				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<h2 class="title-page">
						<?php 
							foreach (get_the_terms(get_the_ID(), 'boletim-tax') as $cat) {
								echo $cat->name;
							}
						?>
					</h2>
				</div>

			</div>

			<div class="row" id="noticias">

					<?php
					if ( have_posts() ) : while ( have_posts() ) : the_post();
				    $tituloBoletim = $post->post_title;
				    $customBoletim = get_post_custom($post->ID);
					?>

					<div class="col-lg-4 col-sm-6 text-center mb-4">
						<a href="<?php echo $customBoletim['wpcf-arquivo-do-boletim'][0]; ?>" target="_blank">
							<i class="fa fa-file-text fa-lg fa-3x"></i>
							<p></p>
							<h3><?php echo $tituloBoletim; ?></h3>
						</a>
					</div>

					<?php endwhile; ?>
					<?php else: ?>
					<?php endif; ?>

					

					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
						<p><?php the_posts_pagination( array( '' ) ); ?></p>
					</div>

			</div>
		</div>
	</div>
