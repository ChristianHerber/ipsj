	<div class="container-fluid">
		<div class="container content-tax">
			<div class="row">

				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<h2 class="title-page">
						<?php 
							foreach (get_the_terms(get_the_ID(), 'categoria-video') as $cat) {
								echo $cat->name.'s';
							}
						?>
					</h2>
				</div>

			</div>

			<div class="row" id="noticias">

					<?php
					if ( have_posts() ) : while ( have_posts() ) : the_post();
				    // $image_id = get_post_thumbnail_id($post->ID);
				    // $image_url = wp_get_attachment_image_src($image_id, '200x200');
				    // $image = $image_url[0];
				    // $tituloNoticias = $post->post_title;
				    // $resumoNoticias = get_the_excerpt($post->ID);
				    // $linkNoticias = get_the_permalink($post->ID);
				    $customVideo = get_post_custom($post->ID);
					?>

					<div class="col-lg-4 col-sm-12 text-center mb-4 embed-responsive embed-responsive-4by3">
						<?php echo $customVideo['wpcf-youtube'][0]; ?>
					</div>

					<?php endwhile; ?>
					<?php else: ?>
					<?php endif; ?>

					

					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
						<p><?php the_posts_pagination( array( '' ) ); ?></p>
					</div>

			</div>
		</div>
	</div>
