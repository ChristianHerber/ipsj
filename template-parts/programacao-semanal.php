	<div class="container-fluid" id="programacao">
		<div class="container">
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center"><h2>Programação Semanal</h2></div>
			</div>
		</div>
	</div>

	<div class="container-fluid programacao-content">
		<div class="container">
			<div class="row">
				<?php
	            global $postProgramacaoSemanal;
	            $argsProgramacaoSemanal = array ( 'post_type'=>'programacao', 'post_per_page'=>1 );
	            $mypostsProgramacaoSemanal = get_posts ($argsProgramacaoSemanal);
	            foreach ( $mypostsProgramacaoSemanal as $postProgramacaoSemanal ): setup_postdata($postProgramacaoSemanal);
	            $custom = get_post_custom( $postProgramacaoSemanal->ID );
	            ?>

				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 coluna">

					<div class="dia">
						<h4>Segunda</h4>
						<?php echo wpautop($custom['wpcf-segunda'][0]); ?>
					</div>

					<div class="dia">
						<h4>Terça</h4>
						<?php echo wpautop($custom['wpcf-terca'][0]); ?>
					</div>

					<div class="dia">
						<h4>Quarta</h4>
						<?php echo wpautop($custom['wpcf-quarta'][0]); ?>
					</div>

				</div>

				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 coluna col-meio">

					<div class="dia">
						<h4>Quinta</h4>
						<?php echo wpautop($custom['wpcf-quinta'][0]); ?>
					</div>

					<div class="dia">
						<h4>Sexta</h4>
						<?php echo wpautop($custom['wpcf-sexta'][0]); ?>
					</div>

					<div class="dia">
						<h4>Sábado</h4>
						<?php echo wpautop($custom['wpcf-sabado'][0]); ?>
					</div>

				</div>

				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 coluna">

					<div class="dia">
						<h4>Domingo</h4>
						<?php echo wpautop($custom['wpcf-domingo'][0]); ?>
					</div>

				</div>

				<?php endforeach; ?>

			</div>
		</div>
	</div>

	<iframe src="http://goo.gl/zP3AhN" style="width:100%; height:50px; border:0" frameborder="no" scrolling="no"></iframe>