	<div class="container-fluid" id="rodape">
		<div class="container">
			<div class="row">
				<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
					<h2>Horários de Culto</h2>

					<?php
		            global $postInformacoes;
		            $argsInformacoes = array ( 'post_type'=>'informacao', 'post_per_page'=>1 );
		            $mypostsInformacoes = get_posts ($argsInformacoes);
		            foreach ( $mypostsInformacoes as $postInformacoes ): setup_postdata($postInformacoes);
		            $customInformacoes = get_post_custom( $postInformacoes->ID );
		            ?>

					<div class="horario">
						<p><?php echo $customInformacoes['wpcf-culto-matutino'][0]; ?></p>
						<p>Culto Matutino</p>
					</div>
					<div class="horario">
						<p><?php echo $customInformacoes['wpcf-culto-vespertino'][0]; ?></p>
						<p>Culto Vespertino</p>
					</div>
					<div class="horario">
						<p><?php echo $customInformacoes['wpcf-culto-coreano'][0]; ?></p>
						<p>Culto em Coreano</p>
					</div>
					<div class="horario">
						<p><?php echo $customInformacoes['wpcf-escola-dominical'][0]; ?></p>
						<p>Escola Dominical</p>
					</div>
				</div>
				<div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12">
					<h2>Atendimento da Secretaria</h2>
					<p><i class="fa fa-clock-o"></i> <?php echo $customInformacoes['wpcf-dia-horario'][0]; ?></p>
					<p><i class="fa fa-envelope"></i> <?php echo $customInformacoes['wpcf-email'][0]; ?></p>
					<p><i class="fa fa-map-marker"></i> <?php echo $customInformacoes['wpcf-endereco'][0]; ?></p>
					<p><i class="fa fa-phone"></i> <?php echo $customInformacoes['wpcf-telefone'][0]; ?></p>

					<h2>Redes Sociais</h2>
					<span>IP da Silva Jardim: </span>
					<div class="fb-like" data-href="https://www.facebook.com/Igreja-Presbiteriana-Silva-Jardim-1189728747712404/" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="false"></div>
					<span>Escola Bíblica Dominical: </span>
					<div class="fb-like" data-href="https://www.facebook.com/EBDIPSILVAJARDIM/?hc_ref=ARQDg5Y4FaDQI0NZdhYgv0RqpmYLwysHsA0wKhGfwQhVe9DVCNZWZYM3tuicJFEXdCM&amp;fref=nf" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="false"></div>
				</div>

				<?php endforeach; ?>

			</div>
		</div>
	</div>