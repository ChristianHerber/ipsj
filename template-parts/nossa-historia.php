	<div class="container-fluid" id="historia">
		<div class="container">
			<!-- <div class="row"> -->

				<!-- <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 sound"> -->
					<!-- <h2>Rádio IPB</h2> -->
					<!-- <?php
		            global $postGravacao;
		            $argsGravacao = array ( 'post_type'=>'gravacao', 'post_per_page'=>1 );
		            $mypostsGravacao = get_posts ($argsGravacao);
		            foreach ( $mypostsGravacao as $postGravacao ): setup_postdata($postGravacao);
		            $custom = get_post_custom( $postGravacao->ID );
		            ?>
						
						<?php echo $custom['wpcf-codigo-iframe'][0]; ?>

		        	<?php endforeach; ?> -->

				<!-- <iframe src="http://goo.gl/zP3AhN" style="width:100%; height:350px; border:0" frameborder="no" scrolling="no"></iframe> -->

				<!-- </div> -->

				<div class="row blog">
					<div class="col-12 text-center mb-4">
						<h2>Informações</h2>
					</div>

					<?php
		            global $postBlog;
		            $argsBlog = array ( 'post_type'=>'post', 'category'=>'noticias', 'post_per_page'=>4, 'numberposts'=>4 );
		            $mypostsBlog = get_posts ($argsBlog);
		            foreach ( $mypostsBlog as $postBlog ): setup_postdata($postBlog);
				    $image_idblog = get_post_thumbnail_id($postBlog->ID);
				    $image_urlblog = wp_get_attachment_image_src($image_idblog, '200x200');
				    $imageBlogDestaque = $image_urlblog[0];
		            $dataBlog = get_the_date( 'd/m/Y', $postBlog->ID);
		            $resumoBlog = get_the_excerpt($postBlog->ID);
		            $linkBlog = get_the_permalink($postBlog->ID);
		            ?>
					
					<div class="col-xl-3 col-sm-12">
						<article>
							<a href="<?php echo $linkBlog; ?>">
								<img class="mb-4 img-fluid" src="<?php echo $imageBlogDestaque; ?>" alt="<?php substr($resumoBlog, 0, 120); ?>">
								<h5><?php echo $dataBlog; ?></h5>
								<p><?php echo substr($resumoBlog, 0, 120); ?>...</p>
								<p><a class="btn btn-success btn-ipsj" href="<?php echo $linkBlog; ?>">Ler mais</a></p>
							</a>
						</article>
					</div>

				<?php endforeach; ?>

					<article class="col-12 text-center mt-4">
						<a href="/category/noticias/" class="btn btn-success btn-ipsj">Mais Informações <i class="fa fa-share"></i></a>
					</article>
				</div>
			<!-- </div> -->
		</div>
	</div>