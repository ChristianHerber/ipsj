	<div class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<h2 class="title-page">Contato</h2>
				</div>

				<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
					<form name="form-contato" class="form-contato" id="form-contato" method="post">
						<div class="form-group">
							<input type="text" name="nome" class="form-control" placeholder="Nome">
						</div>
						<div class="form-group">
							<input type="email" name="email" class="form-control" placeholder="seu@email.com" required="required">
						</div>
						<div class="form-group">
							<input type="text" name="assunto" class="form-control" placeholder="Motivo do contato">
						</div>
						<div class="form-group">
							<textarea name="msg" class="form-control" rows="10" placeholder="Digite sua mensagem aqui..."></textarea>
						</div>
						<div class="form-group">
							<button class="btn btn-success btn-lg btn-ipsj">Enviar</button>
						</div>
					</form>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
					<p><i class="fa fa-clock-o"></i> Segunda a Sexta das 08:30h às 12:30h - 13:30h às 17:30h</p>
					<p><i class="fa fa-envelope"></i> ipbsilvajardim@hotmail.com</p>
					<p><i class="fa fa-map-marker"></i> Av. Silva Jardim, 4155 - Seminário, Curitiba - PR, 80240-021</p>
					<p><i class="fa fa-phone"></i> (41)3242-8375</p>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3602.5946306045785!2d-49.301693284791995!3d-25.451812639988056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce39987b28571%3A0x219b34ef05bf07c5!2sIgreja+Presbiteriana+do+Brasil!5e0!3m2!1spt-BR!2sbr!4v1513099713222" width="100%" height="235" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>

			</div>
		</div>
	</div>