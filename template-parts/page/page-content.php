	<div class="container-fluid">
		<div class="container">
			<div class="row">

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<h2 class="title-page"><?php the_title(); ?></h2>
				</div>

				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<?php
						$content = the_content();
						do_shortcode(wpautop($content));
					?>
				</div>

				<?php endwhile; ?>
				<?php else: ?>
				<?php endif; ?>

			</div>
		</div>
	</div>