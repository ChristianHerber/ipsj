	<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
	  <div class="carousel-inner">
		<?php
		    $i=1;
		    global $postslide;
		    $argsslide = array ( 'post_type'=>'slide', 'post_per_page'=>-1, 'numberposts'=>-1 );
		    $mypostsslide = get_posts ($argsslide);
		    foreach ( $mypostsslide as $postslide ): setup_postdata($postslide);
		    $image_idslide = get_post_thumbnail_id($postslide->ID);
		    $image_urlslide = wp_get_attachment_image_src($image_idslide, 'slide');
		    $imageSlideDestaque = $image_urlslide[0];
		    $tituloSlide = $postslide->post_title;
		    $contentSlide = $postslide->post_content;
		    $customSlide = get_post_custom($postslide->ID);
		?>

		    <div class="carousel-item <?php if ($i == 1) echo 'active'; ?>">
		      <a href="<?php echo $customSlide['wpcf-link-do-banner'][0]; ?>">
		      	<img class="d-block w-100" src="<?php echo $imageSlideDestaque; ?>" alt="<?php echo $$tituloSlide; ?>">
		      </a>
		    </div>

		<?php $i++; endforeach; ?>

	  </div>
	</div>