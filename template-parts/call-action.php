	<div class="container-fluid" id="call">
		<div class="container">
			<div class="row">
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
					<h2>Agenda Anual</h2>
					<?php
		            global $postAgenda;
		            $argsAgenda = array ( 'post_type'=>'agenda', 'post_per_page'=>1 );
		            $mypostsAgenda = get_posts ($argsAgenda);
		            foreach ( $mypostsAgenda as $postAgenda ): setup_postdata($postAgenda);
		            $custom = get_post_custom( $postAgenda->ID );
		            ?>
					
					<a href="<?php echo $custom['wpcf-agenda-pdf'][0]; ?>" target="_blank" class="btn btn-lg btn-success btn-ipsj btn-ipsj-inverse">Clique Aqui</a>

					<?php endforeach; ?>
				</div>
				<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
					<!-- <h2 class="tema">Tema:</h2>
					<p>Jesus Cristo, sua humanidade, sua espiritualidade e seu sacrifício</p> -->
				</div>
			</div>
		</div>
	</div>