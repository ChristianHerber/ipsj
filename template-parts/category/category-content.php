	<div class="container-fluid">
		<div class="container">
			<div class="row">

				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<h2 class="title-page">
						<?php 
							foreach (get_the_category() as $cat) {
								echo $cat->name;
							}
						?>
					</h2>
				</div>

			</div>

			<div class="row" id="noticias">

					<?php
					if ( have_posts() ) : while ( have_posts() ) : the_post();
				    $image_id = get_post_thumbnail_id($post->ID);
				    $image_url = wp_get_attachment_image_src($image_id, '200x200');
				    $image = $image_url[0];
				    $tituloNoticias = $post->post_title;
				    $resumoNoticias = get_the_excerpt($post->ID);
				    $linkNoticias = get_the_permalink($post->ID);
					?>

					<div class="col-lg-4 col-sm-6 text-center mb-4">
						<a href="<?php echo $linkNoticias; ?>">
							<img class="rounded-circle img-fluid d-block mx-auto cat-img" src="<?php echo $image; ?>" alt="<?php echo $tituloNoticias; ?>">
							<h3><?php echo substr($tituloNoticias, 0, 30); ?></h3>
							<p><?php echo substr($resumoNoticias, 0, 75); ?>...</p>
						</a>
					</div>

					<?php endwhile; ?>
					<?php else: ?>
					<?php endif; ?>

					

					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
						<p><?php the_posts_pagination( array( '' ) ); ?></p>
					</div>

			</div>
		</div>
	</div>
