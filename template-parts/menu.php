	<div class="container-fluid">
		<div class="">
			<div class="row border">
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 logo">
					<a href="http://ipsj.org.br">
						<img src="<?php bloginfo('template_url'); ?>/img/logo-ipsj.png" alt="Igreja Presbiteriana Silva Jardim" class="img-fluid">
					</a>
				</div>
				<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 menu">
					<nav class="navbar navbar-expand-lg">
						<div class="container">
							<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"><i class="fa fa-bars fa-lg fa-2x"></i></span>
							</button>

							<div class="collapse navbar-collapse" id="navbarNav">
								<ul class="navbar-nav mr-auto">
									<?php if (function_exists('getNavMenu')): ?>
							        	<?php echo getNavMenu('menutopo','hover'); ?>
							        <?php endif; ?>
								</ul>
							</div>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</div>