<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Igreja Presbiteriana da Silva Jardin - Curitiba PR</title>
	<meta name="description" content="A Igreja Presbiteriana do Brasil é uma igreja protestante, reformada e de orientação calvinista presbiteriana, ou seja, é governada por um conselho de homens escolhidos pela comunidade dos membros, chamados de “presbíteros”.">
	<meta name="keywords" content="igreja, presbiteriana, brasil, curitiba, jesus, evangelho, biblia, reforma, protestante">
	<meta name="author" content="Uebi! Agência Digital">
	<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/fontawesome.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/style.css">
	<link rel="icon" href="<?php bloginfo('template_url') ?>/img/favicon.png">
	<?php wp_head(); ?>
</head>
<body>